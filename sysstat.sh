#!/bin/bash
#     Author: Sanju P Debnath
#   Git Link: https://gitlab.com/linuxcli/sysstat
# ScriptName: SystemStat

Version="0.6.9"

WELCOME=$(WEL="$(echo -e "Welcome to System Stats $Version")";ICHAR="$WEL";CHARCOUNT="$(echo $(($(expr length "$ICHAR")+4)))";DASH=$(for A in $(seq $CHARCOUNT);do echo -n  "-";done);echo -e "\n\x20\x20\x20$DASH\n\x20\x20\x20| \e[1;31m$ICHAR\e[0m |\n\x20\x20\x20$DASH\n")

	function hlp {

		echo -e "

	-----------
	| SysStat |
	-----------

	Display information about System Status.
	Below are the available switches and there usage. 

	\e[1mhelp\e[0m	- To view manual of this script.

	\e[1mver\e[0m	- Get version and other info.

	To view specific output use below switches for details.

	\e[1mshort\e[0m	- Short switch provide summary details of system.
	\e[1mla\e[0m	- Load average details minute ago.
	\e[1mcpu\e[0m	- Processor details and top 5 CPU consuming process.
	\e[1mmem\e[0m	- Physical and swap memory details and current top 5 memory consuming process.
	\e[1mnet\e[0m	- Network devices and there IP Address.
	\e[1mpro\e[0m	- Top 5 memory and CPU consuming process.
	\e[1mhost\e[0m	- Hostname, running kernel, OS version and current system manager info. 
		  For system manager info \e[1mhost\e[0m switch is require to be run with root privilege.
	\e[1mdisk\e[0m	- Info about the disk and mounted devices.
	\e[1mh\e[0m	- Only display hostname
	\e[1mall\e[0m	- To get all info about current system.
	\e[1mnc\e[0m	- Monitor Internet connection with 3 second interval.
	\e[1mnc log\e[0m	- Redirect output to a file under the path "/tmp/netc/Year-Month-DayoftheMonth.log".
	          To cancel kindly press Ctrl + C combination keys.
		  To keep it running like a daemon then kindly add ampersand (&) at last.

		Example:    \` \e[1msysstat.sh  help \e[0m'
		Run this script without any switch you will get help page.

	\n"
exit 0
}

hlpmsg () {
TIME="0.05"

MSG=$(echo "$(hlp)")
LNCOUNT=$(echo "$MSG"|wc -l)

for LINE in $(seq $LNCOUNT)
do 
ELINE=$(printf "$MSG"|sed -n "$LINE p")
CHCOUNT=$(printf "$ELINE"|wc -L)

for CHAR in $(seq $CHCOUNT)
do
printf "$(printf "$ELINE"|cut -b$CHAR)"
sleep $TIME
done
printf "\n"
done

exit 0
}

function ver {
		echo -e "
	Running version \e[1;31m$Version\e[0m
	ScriptName: \e[1;31mSystemStats\e[0m
	Author: Sanju P Debnath
	Contact: \e[4mhttps://discus.linuxcli.in\e[0m
		\n"
exit 0
}

if [[ "$1" == "help" ]]
then
clear
	hlpmsg

elif [[ "$1" == "" ]]
then
clear
	hlp

elif [[ "$1" == "ver" ]]
then
		ver
fi


function welcome {
 echo -e "\e[0m"
	clear
	echo -e "$WELCOME \n"
}

function short {

HOST=$(uname -n)
OSNAME=$(awk -F\= '/PRETTY_NAME/{print $2}' /etc/*release|tr -d "\"")
KRNL=$(uname -r)
ARCH=$(uname -m)
PROC=$(awk -F: '/cpu cores/{print $2}' /proc/cpuinfo|uniq)

CTOP=$(ps h -eo comm --sort -%cpu)
CTOP1=$(echo "$CTOP"|sed -n '1p')
CTOP2=$(echo "$CTOP"|sed -n '2p')
CTOP3=$(echo "$CTOP"|sed -n '3p')

MTOP=$(ps h -eo comm --sort -%mem)
MTOP1=$(echo "$MTOP"|sed '1!d')
MTOP2=$(echo "$MTOP"|sed '2!d')
MTOP3=$(echo "$MTOP"|sed '3!d')

FrMEM=$(($(awk '/MemFree/{print $2}' /proc/meminfo)*100/$(awk '/MemTotal/{print $2}' /proc/meminfo)))
LAV=$(cat /proc/loadavg)
LA1M=$(echo $LAV|awk '{print $1}')
LA2M=$(echo $LAV|awk '{print $2}')
LA3M=$(echo $LAV|awk '{print $3}')

function net {
 if [ $(which ip) ]
	then 
NETDEVLIST="$(ip addr| awk '/: /{print $2}'|tr -d ":"|tr -d "[:blank:]"|awk -F\@ '{print $1}'|sed '/lo/d')"

 for DEV in $NETDEVLIST
  do
IPV4="$(ip -s addr show $DEV|awk '/inet\ /{print $2}'|sed -n '1p'|awk -F"/" '{print $1}')"
IPV6="$(ip -s addr show $DEV|awk '/inet6/{print $2}'|sed -n '1p'|awk -F"/" '{print $1}')"

printf  "%-15s\e[1m$DEV\e[0m\n%-15s$(if [ $IPV4 ];then printf "IPv4: $IPV4";fi)%-3s$(if [ $IPV6 ];then printf "IPv6: $IPV6";fi)
"
 done

 elif [ $(which ifconfig) ]
   then
NETDEVLIST="$(ifconfig -a|awk '/: /{print $1}'|tr -d ":"|sed '/lo/d'|tac)"

   for DEV in $NETDEVLIST
    do
IPV4="$(ifconfig $DEV|awk '/inet\ /{print $2}'|sed -n '1p')"
IPV6="$(ifconfig $DEV|awk '/inet6/{print $2}'|sed -n '1p')"

printf "%-15s\e[1m$DEV\e[0m\n%-15s$(if [ $IPV4 ];then printf "IPv4: $IPV4";fi)%-3s$(if [ $IPV6 ];then printf "IPv6: $IPV6";fi)
"
 done
 else
printf  "\n   Binary \"\e[1mip\e[0m\" or \"\e[1mifconfig\e[0m\" not found\n   Please install package \e[1miproute2\e[0m or \e[1mnet-tools\e[0m"
  fi
 }

DISK=$(df -hT|awk '$1 !~ /tmpfs|udev|cgroup|sysfs|proc|devpts|securityfs|pstore|systemd|debugfs|hugetlbfs|mqueue|binfmt_misc/ {print}'|sed '1d')
COUNT=$(echo "$DISK"|wc -l)
FRSPC="$(for S in $(seq $COUNT);do  echo "%-14s $((100 -$(echo "$DISK"|sed -n "$S p"|sed 's/%//g'|awk '{print $6}')))%% %-10s  $(echo "$DISK"|sed -n "$S p"|awk '{print $7}')" ;done )"

printf "
  Host:%-8s\e[1m$HOST\e[0m
  OS:%-10s\e[1m$OSNAME\e[0m
  Kernel:%-5s \e[1m$KRNL\e[0m
  Arch:%-7s \e[1m$ARCH\e[0m
  Processor:%-2s\e[1m$PROC\e[0m core\t  Top 3 process: \"\e[1;31m$CTOP1\e[0m\", \"\e[1m$CTOP2\e[0m\", \"\e[1m$CTOP3\e[0m\"
  Memory:%-6s\e[1m$FrMEM\e[0m%% free\t  Top 3 process: \"\e[1;31m$MTOP1\e[0m\", \"\e[1m$MTOP2\e[0m\", \"\e[1m$MTOP3\e[0m\"
  Load Avg:%-4s\"\e[1m$LA1M\e[0m\" 1 min, \"\e[1m$LA2M\e[0m\" 5 min, \"\e[1m$LA3M\e[0m\" 15 min ago
  IP Address:%-1s\n$(net)\n
  Partitions:%-1s Free  %-9s Mount
%-14s Space %-9s Point
%-14s ----- %-9s -----
$FRSPC

 \n"

 }

if [[ "$1" == "short" ]]
 then
	welcome
	short
exit 0
fi


CHARCOUNT=50
function DSH0 {
 for A in $(seq $CHARCOUNT)
	do echo -n  "-"
 done
 }

 function DASH {
	 echo -e "\n $(DSH0) \n"
 }


echo -e  "$DASH"

# Load Average
# ------------
function la {
FILE='/proc/loadavg'

echo -e " Load since 
        1 min ago \e[1;31m$(awk '{print $1}' $FILE)\e[0m
        5 min ago \e[1;31m$(awk '{print $2}' $FILE)\e[0m
       15 min ago \e[1;31m$(awk '{print $3}' $FILE)\e[0m
       "
echo $LOAD

}

# Processor 
# ---------
function cpu {
FILE="/proc/cpuinfo"
SOCKET="$(awk -F: '/physical id/{print $2}' $FILE|uniq|wc -l)"
CORE="$(awk -F: '/cpu cores/{print $2}' $FILE|uniq)"
CACHE="$(awk -F: '/cache size/{print $2}' $FILE|uniq)"
MODEL="$(awk -F: '/model name/{print $2}' $FILE|awk -F@ '{print $1}'|uniq)"
CLOCK="$(awk -F: '$1 ~ /MHz|GHz/{print $2}' $FILE|uniq)"
FREQ="$(awk '$2 ~ /MHz|GHz/{print $2}' $FILE|uniq)"

printf " Processor
 ---------
 Model	     : $MODEL
 No. Socket  :  $SOCKET
 No. Core    : $CORE
 Clock Speed : $CLOCK $FREQ
 Cache Size  : $CACHE
\n"
}

# Memory
# ------
function MEM {
FILE="/proc/meminfo"
SFILE="/proc/swaps"
TMEM="$(awk '/MemTotal/{print $2}' $FILE)"
CMEM="$(awk '/^Cached/{print $2}' $FILE)"
BMEM="$(awk '/^Buffers/{print $2}' $FILE)"
FMEM="$(awk '/MemFree/{print $2}' $FILE)"
AMEM="$(awk '/MemAvail/{print $2}' $FILE)"
ST="$(awk '/SwapTotal/{print $2}' $FILE)"
SFRE="$(awk '/SwapFree/{print $2}' $FILE)"

TMEML="$(expr length $TMEM)"
FMEML="$(expr length $FMEM)"
BMEML="$(expr length $BMEM)"
CMEML="$(expr length $CMEM)" 
STL="$(expr length $ST)"
SFREL="$(expr length $SFRE)"

TRAM="$(if [[ $TMEML > 7 ]];then   echo -e "$(($TMEM/1024**2)) GiB";else  echo -e "$(($TMEM/1024)) MiB";fi)"
FRRAM="$(if [[ $FMEML > 7 ]];then  echo -e "$(($FMEM/1024**2)) GiB";else  echo -e "$(($FMEM/1024)) MiB";fi)"
BRAM="$(if [[ $BMEML > 7 ]];then   echo -e "$(($BMEM/1024**2)) GiB";else  echo -e "$(($BMEM/1024)) MiB";fi)"
CRAM="$(if [[ $CMEML > 7 ]];then   echo -e "$(($CMEM/1024**2)) GiB";else  echo -e "$(($CMEM/1024)) MiB";fi)"
TSWP="$(if [[ $STL > 7 ]];then     echo -e "$(($ST/1024**2)) GiB";else  echo -e "$(($ST/1024)) MiB";fi)"
FSWP="$(if [[ $SFREL > 7 ]];then   echo -e "$(($SFRE/1024**2)) GiB";else  echo -e "$(($SFRE/1024)) MiB";fi)"
SWPDEV="$(awk '{print $1}' /proc/swaps |sed '1d'|sed -z 's/\n/, /g'|awk '{sub(", $","");print}')"

SWD="$(sed '1d' /proc/swaps |tail -n1|awk '{print $1}'|wc -l)"

if [ $SWD == 0 ]
then
printf "  RAM
 ---
  Total : $TRAM
  Free  : $(($FMEM*100/$TMEM))%%	
  Buffer: $(($BMEM*100/$TMEM))%%
  Cached: $(($CMEM*100/$TMEM))%%

"
else
printf "  RAM	%-8s	Swap-Memory
  --- %-12s	-----------
  Total : $TRAM %-4s	$TSWP
  Free  : $(($FMEM*100/$TMEM))%%  %-4s	$(($SFRE*100/$ST))%%		
  Buffer: $(($BMEM*100/$TMEM))%%  %-4s
  Cached: $(($CMEM*100/$TMEM))%%  %-4s
  Device:	%-4s	$SWPDEV

"
 fi    
}

# Current running system and conf manager
# ---------------------------------------
SYSMGRMSG="System Manager : %-4s" 
SYSMGR=$(ps -p 1 -o 'comm=' | tr [a-z] [A-Z])
function SyS {
   printf  " $SYSMGRMSG\e[1;31m$SYSMGR\e[0m \n"
}

function SM {
	CHARCOUNT="$(echo $(( $(expr length "$(echo "$SYSMGRMSG $SYSMGR")") + 4)))"
SYSDSH=$(for A in $(seq $CHARCOUNT);do echo -n  "-";done)
	printf  "\n\x20$SYSDSH\n\x20| System Manager : %-4s \e[1;31m$SYSMGR\e[0m |\n\x20$SYSDSH\n\n"
}

# Top CPU consuming process
# -------------------------
function tcpu {
PSCPU="$(ps --cols 50 h -e -o %cpu,cmd --sort -%cpu|sed '1,5!d'|awk '{print "\x20",$1,"\x20\x20\x20",$2}')"
echo -e "\x20Top 5 processor consuming process"
echo -e "$DASH"
echo -e "\e[1m\x20\x20Usage%  Executable\e[0m"
echo -e "\e[31m$PSCPU" | sed '1!d'
echo -ne "\e[0m"
echo "$PSCPU"|sed '2!d'
echo "$PSCPU"|sed '3!d'
echo "$PSCPU"|sed '4!d'
echo "$PSCPU"|sed '5!d'
echo -e ""
}

# Top Mem cosuming process
# ------------------------
function tmem {
PSMEM="$(ps --cols 50 h -e -o %mem,cmd --sort -%mem|sed '1,5!d'|awk '{print "\x20",$1,"\x20\x20\x20",$2}')"
echo -e "\x20Top 5 memory consuming process"
echo -e "$DASH"
echo -e "\e[1m\x20\x20Usage%  Executable\e[0m"
echo -e "\e[31m$PSMEM" | sed '1!d'
echo -ne "\e[0m"
echo "$PSMEM"|sed '2!d'
echo "$PSMEM"|sed '3!d'
echo "$PSMEM"|sed '4!d'
echo "$PSMEM"|sed '5!d'
echo -e ""
}

function HOST {
printf "\e[0m Hostname      : %4s \e[1;31m$(uname -n)\e[0m
\e[0m OS Name       : %4s \e[1;31m$(AB=$(awk -F\( '{print $4}' /proc/version|awk -F\) '{print $1}');echo $AB |sed "s/$(echo $AB|awk '{print $NF}')//g")\e[0m
                 %4s \e[1;96m$(awk -F\= '/PRETTY_NAME/{print $2}' /etc/*release|tr -d "\"")\e[0m
\e[0m Architecture  : %4s \e[1;31m$(uname -m)\e[0m
\e[0m Linux Version : %4s \e[1;31m$(uname -r)\e[0m\n"
}

function SHTHST {
printf "\e[0m Hostname      : %4s \e[1;31m$(uname -n)\e[0m\n\n"
}

NETMSG=$(echo -e " Network Device Status:\n
 If network device name is green in colour then it means IP address is assinged or
 if it is red in color then it means it offline on network")

function NET {
	if [ $(which ip) ]
then 
NETDEVLIST="$(ip addr| awk '/: /{print $2}'|tr -d ":"|awk -F\@ '{print $1}')"

for DEV in $NETDEVLIST

do
IPV4_1="$(ip -s addr show $DEV|awk '/inet\ /{print $2}'|sed -n '1p')"
IPV4_2="$(ip -s addr show $DEV|awk '/inet\ /{print $2}'|sed -n '2,$p')"
IPV6_1="$(ip -s addr show $DEV|awk '/inet6/{print $2}'|sed -n '1p')"
IPV6_2="$(ip -s addr show $DEV|awk '/inet6/{print $2}'|sed -n '2,$p')"

OPIPV4_2="$(for S in $IPV4_2;do echo -e "                     $S";done)"
OPIPV6_2="$(for S in $IPV6_2;do echo -e "                     $S";done)"


INET="$DEV"
INETCOUNT="$(echo $(($(expr length "$INET")+2)))"
NETDH=$(for A in $(seq $INETCOUNT);do echo -n  "-";done)

echo -e  "\n$NETDH\n|$(if [ $IPV4_1 ] || [ $IPV6_1 ]
then 
	echo -e "\e[1;32m$DEV\e[0m"
else 
	echo -e "\e[1;31m$DEV\e[0m"
fi)|\n$NETDH"

printf "\n Physical Address  : $(ip -s addr show  $DEV |awk '$1 ~ /link/ {print $2}')"

if [ $IPV4_1 ] || [ $IPV6_1 ]
then
printf "\n\n Logical Address =>"
fi
if [ $IPV4_1 ]
then
printf "\n              IPv4 : $IPV4_1
$OPIPV4_2"
fi
if [ $IPV6_1 ]
then
printf "\n              IPv6 : $IPV6_1
$OPIPV6_2"
fi
RPC="$(($(ip -s addr show $DEV |cat -n|awk '/RX/{print $1}')+1))"
SPC="$(($(ip -s addr show $DEV |cat -n|awk '/TX/{print $1}')+1))"

DRPC="$(ip -s addr show $DEV | sed -n "$RPC p"|awk '{print $4}')"
DSPC="$(ip -s addr show $DEV | sed -n "$SPC p"|awk '{print $4}')"
ERPC="$(ip -s addr show $DEV | sed -n "$RPC p"|awk '{print $3}')"
ESPC="$(ip -s addr show $DEV | sed -n "$SPC p"|awk '{print $3}')"

if [ $DRPC != 0 ] || [ $DSPC != 0 ]
 then
 printf "\n\n Dropped Packets =>"
fi
 if [ $DRPC != 0 ]
 then
printf "\n         Received  : $DRPC"
fi
 if [ $DSPC != 0 ]
 then
printf "\n         Sent      : $DSPC"
	 fi

if [ $ERPC != 0 ] || [ $ESPC != 0 ]
then
printf "\n\n Error Packets   =>" 
fi
 if [ $ERPC != 0 ] 
then printf "\n         Received  : $ERPC"
fi
 if [ $ESPC != 0 ]
 then printf "\n         Sent      : $ESPC"
 fi
printf "\n\n"
done

elif [ $(which ifconfig) ]
then
NETDEVLIST="$(ifconfig -a|awk '/: /{print $1}'|tr -d ":"|tac)"
for DEV in $NETDEVLIST

do
IPV4_1="$(ifconfig $DEV|awk '/inet\ /{print $2}'|sed -n '1p')"
IPV4_2="$(ifconfig $DEV|awk '/inet\ /{print $2}'|sed -n '2,$p')"
IPV6_1="$(ifconfig $DEV|awk '/inet6/{print $2}'|sed -n '1p')"
IPV6_2="$(ifconfig $DEV|awk '/inet6/{print $2}'|sed -n '2,$p')"

OPIPV4_2="$(for S in $IPV4_2;do echo -e "                     $S";done)"
OPIPV6_2="$(for S in $IPV6_2;do echo -e "                     $S";done)"

INET="$DEV"
INETCOUNT="$(echo $(($(expr length "$INET")+2)))"
NETDH=$(for A in $(seq $INETCOUNT);do echo -n  "-";done)

echo -e  "\n$NETDH\n|$(if [ $IPV4_1 ] || [ $IPV6_1 ]
then 
	echo -e "\e[1;32m$DEV\e[0m"
else 
	echo -e "\e[1;31m$DEV\e[0m"
fi)|\n$NETDH"

printf "\n Physical Address  : $(ifconfig $DEV |awk '/ether/ {print $2}')"

if [ $IPV4_1 ] || [ $IPV6_1 ]
then
printf "\n\n Logical Address =>"
fi
if [ $IPV4_1 ]
then
printf "\n              IPv4 : $IPV4_1
$OPIPV4_2"
fi
if [ $IPV6_1 ]
then
printf "\n              IPv6 : $IPV6_1
$OPIPV6_2"
fi

DRPC="$(ifconfig $DEV|sed -n '/TX/p'|awk '/dropped /{print $5}')"
DSPC="$(ifconfig $DEV|sed -n '/RX/p'|awk '/dropped /{print $5}')"
ERPC="$(ifconfig $DEV|sed -n '/TX/p'|awk '/errors /{print $3}')"
ESPC="$(ifconfig $DEV|sed -n '/RX/p'|awk '/errors /{print $3}')"

if [ $DRPC != 0 ] || [ $DSPC != 0 ]
 then
 printf "\n\n Dropped Packets =>"
fi
 if [ $DRPC != 0 ]
 then
printf "\n         Received  : $DRPC"
fi
 if [ $DSPC != 0 ]
 then
printf "\n         Sent      : $DSPC"
	 fi

if [ $ERPC != 0 ] || [ $ESPC != 0 ]
then
printf "\n\n Error Packets   =>" 
fi
 if [ $ERPC != 0 ] 
then 
printf "\n         Received  : $ERPC"
fi
 if [ $ESPC != 0 ]
 then 
printf "\n         Sent      : $ESPC"
 fi
printf "\n\n"
done

else
	printf  "\n   Binary \"\e[1mip\e[0m\" or \"\e[1mifconfig\e[0m\" not found\n   Please install package \e[1miproute2\e[0m or \e[1mnet-tools\e[0m"
fi

}

function nchk {
if [ $(which ping) ]
then
DOMAIN="google.com"
IP="8.8.8.8"
INTERVAL=3	#Value in seconds
TS=$(date +%y-%m-%d" | "%H:%M:%S)

for (( ; ; ))
do

TS=$(date +%y-%m-%d" | "%H:%M:%S)
RDV=$(ping -c4 -W5 $DOMAIN 2> /dev/null|awk -F\, '/packet loss/{print $3}')
RD=$(echo $RDV|awk -F\% '{print $1}'|tr -d '[:space:]')
if [[ "$RD" == "0" ]]
then
	printf "\nDt:$TS\n\x09DNS query \e[1;32mresolved\e[0m.\n"
	printf "\x09$(for A in $(seq 19);do printf "-";done)\n"
else
	printf "\n$TS\n  \e[1;31mFailed\e[0m to resolve DNS query.\n  Please check DNS setttings.\n"
	echo "  $(for A in $(seq 45);do echo -n "-";done)"
fi

ICV=$(ping -c4 -W5 $IP 2> /dev/null|awk -F\, '/packet loss/{print $3}')
IC=$(echo $ICV|awk -F\% '{print $1}'|tr -d '[:space:]')


if [[ "$IC" == "0" ]]
then
	printf "\x09\e[1;32mOnline\e[0m on Internet.\n"

else
	printf "  Internet gone \e[1;31mOffline\e[0m.\n  Please check network interface configuration.\n"
	echo ""
fi

	sleep $INTERVAL
done
else
printf "ping executable not found\n"
sleep 0.5
printf "\n"
fi
}

function nchk_log {
if [ $(which ping) ]
then
DOMAIN="google.com"
IP="8.8.8.8"
INTERVAL=3	#Value in seconds
TS=$(date +%y-%m-%d" | "%H:%M:%S)
FN=$(date +%Y-%m-%d)
LOGDIR="/tmp/netc/"
FILE="$LOGDIR/$FN.log"

for (( ; ; ))
do

TS=$(date +%y-%m-%d" | "%H:%M:%S)
RDV=$(ping -c4 -W5 $DOMAIN 2> /dev/null|awk -F\, '/packet loss/{print $3}')
RD=$(echo $RDV|awk -F\% '{print $1}'|tr -d '[:space:]')

if [[ "$RD" == "0" ]]
then
	printf "\nDt:$TS\n\x09DNS query resolved.\n"
	printf "\x09$(for A in $(seq 19);do printf "-";done)\n"
else
	printf "\n$TS\n  Failed to resolve DNS query.\n  Please check DNS setttings.\n"
	echo "  $(for A in $(seq 45);do echo -n "-";done)"
fi

ICV=$(ping -c4 -W5 $IP 2> /dev/null|awk -F\, '/packet loss/{print $3}')
IC=$(echo $ICV|awk -F\% '{print $1}'|tr -d '[:space:]')


if [[ "$IC" == "0" ]]
then
	printf "\x09Online on Internet.\n"

else
	printf "  Internet gone Offline.\n  Please check network interface configuration.\n"
	echo ""
fi

	sleep $INTERVAL
done
else
printf "ping executable not found\n"
sleep 0.5
printf "\n"
fi
}


function dsk {

	function DAT {
		awk '$NF !~ /[0-9]$/{print $3,"",$4}' /proc/partitions|sed '1,2d'
	}
	
COUNT=$(DAT|wc -l)		
DISK=$(for S in $(seq $COUNT);do DAT|awk '{print "/dev/"$2}'|sed -n "$S p"|sed -z 's/\n/%-4s/g';done)

function SIZE {
for S in $(seq $COUNT)
do 
SZ=$(DAT|awk '{print $1}'|sed -n "$S p")
SZLG=$(expr length $SZ)
 if [ $SZLG -ge 10 ]
 then
	printf "$(($(DAT|awk '{print $1}'|sed -n "$S p")/1024/1024/1024)) TiB%-6s"
 else
	printf "$(($(DAT|awk '{print $1}'|sed -n "$S p")/1024/1024)) GiB%-6s"
fi
 done
}
 printf " Disks :  $DISK
 Size  :  $(SIZE) \n"
	}

	function MNT {

MOUNT=$(awk '$1 ~ /\// {print}' /proc/mounts)
COUNT=$(printf "$MOUNT\n"|wc -l)
for A in $(seq $COUNT)
	do 
DRIVE=$(printf "$MOUNT"|awk '{print $1}'|sed -n "$A p")
MPOINT=$(printf "$MOUNT"|awk '{print $2}'|sed -n "$A p")
FSYS=$(printf "$MOUNT"|awk '{print $3}'|sed -n "$A p")
USESPC=$(df $(printf "$DRIVE")|sed '1d'|awk '{print $5}'|tr -d "%")
FRSPC=$((100-$USESPC))

printf " Drives%-6s:%-2s$DRIVE
 File System%-1s:%-2s$FSYS
 Mount Point%-1s:%-2s$MPOINT
 Free Space%-2s:%-2s$FRSPC%%
"
printf "\n"
done
}

if [[ "$1" == "mem" ]]
 then
	 welcome
	 MEM
	 DASH
	 tmem

elif [[ "$1" == "cpu" ]]
then
	welcome
	cpu
	DASH
	tcpu

elif [[ "$1" == "net" ]]
then
	welcome
	printf "$NETMSG\n"
	NET

elif [[ "$1" == "nc" ]]
then
FN=$(date +%Y-%m-%d)
LOGDIR="/tmp/netc/"
FILE="$LOGDIR/$FN.log"
if  [[ "$2" == "log" ]]
then
  mkdir -p "$LOGDIR"
	    nchk_log >> "$FILE"
    else
	    nchk
    fi

elif [[ "$1" == "disk" ]]
then
	welcome
	dsk
	DASH
	MNT

elif [[ "$1" == "pro" ]]
then
	welcome
	tcpu
	DASH
	tmem

elif [[ "$1" == "la" ]]
then
	welcome
	la

elif [[ "$1" == "host" ]]
then
	welcome
	HOST
	SM


elif [[ "$1" == "h" ]]
then
        welcome
        SHTHST

elif [[ "$1" == "all" ]]
then
	welcome
	HOST
	DASH
	SyS
	DASH
	cpu
	DASH
	MEM
	DASH
	tcpu
	DASH
	tmem
	DASH
	NET
	DASH
	dsk
	DASH
	MNT

else
	printf  "  $1  \e[31minvalid switch\e[0m\n  Kindly try \"\e[1;32msysstat.sh help\e[0m\" for more info. \n \n"
exit 34
	fi
